%%% dsc.erl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2015-2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @doc This library module implements the public API for the
%%% 	{@link //dsc. dsc} application.
%%%

-module(dsc).

%% export the dsc public API
-export([add_service/5, find_service/1, delete_service/1, start_service/3,
		add_peer/4, find_peer/1, delete_peer/1,
		add_realm/5, find_realm/1, delete_realm/1]).

-include("dsc.hrl").

-spec add_service(Name, Address, Port, Options, Enabled) -> Result
	when
		Name :: term(),
		Address :: inet:ip_address(),
		Port :: inet:port_number(),
		Options :: [tuple()],
		Enabled :: boolean(),
		Result :: {ok, #dsc_service{}} | {error, Reason},
		Reason :: term().
%% @doc Create an entry in `dsc_service' table.
%%
add_service(Name, Address, Port, Options, Enabled) when is_list(Address),
		is_list(Options), is_boolean(Enabled) ->
	case inet:parse_address(Address) of
		{ok, Addr} ->
			add_service(Name, Addr, Port, Options, Enabled);
		{error, Reason} ->
			{error, Reason}
	end;
add_service(Name, Address, Port, Options, Enabled) when is_tuple(Address),
		is_list(Options), is_boolean(Enabled) ->
	F = fun() ->
				R = #dsc_service{name = Name, address = Address, port = Port,
						enabled = Enabled, options = Options},
				ok = mnesia:write(R),
				R
	end,
	case mnesia:transaction(F) of
		{atomic, Service} ->
			{ok, Service};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec find_service(Name) -> Result
	when
		Name :: term(),
		Result :: {ok, #dsc_service{}} | {error, Reason},
		Reason :: not_found | term().
%% @doc Find a service  by name.
%%
find_service(Name) ->
	F = fun() ->
				mnesia:read(dsc_service, Name, read)
	end,
	case mnesia:transaction(F) of
		{atomic, [#dsc_service{} = Service]} ->
			{ok, Service};
		{atomic, []} ->
			{error, not_found};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec delete_service(Name) -> ok
	when
		Name :: term().
%% @doc Delete an entry from the `dsc_service' table.
delete_service(Name)  ->
	F = fun() ->
		mnesia:delete(dsc_service, Name, write)
	end,
	case mnesia:transaction(F) of
		{atomic, _} ->
			ok;
		{aborted, Reason} ->
			exit(Reason)
	end.

-spec add_peer(Id, Active, Expiration, Transport) -> Result
	when
		Id :: string() | binary() ,
		Active :: boolean(),
		Expiration :: calendar:datetime(),
		Transport ::  tcp | sctp | tls_tcp | dtls_sctp,
		Result :: {ok, #dsc_peer{}} | {error, Reason},
		Reason :: term().
%% @doc Create an entry in `dsc_peer' table.
%%
add_peer(Id, Active, Expr, Trans) when is_list(Id), is_boolean(Active),
		is_tuple(Expr), is_atom(Trans) ->
	add_peer(list_to_binary(Id), Active, Expr, Trans); 
add_peer(Id, Active, Expr, Trans) when is_binary(Id), is_boolean(Active),
		is_tuple(Expr), is_atom(Trans) ->
	F = fun() ->
				R = #dsc_peer{identity = Id, active = Active,
						expiration = Expr, transport = Trans},
				ok = mnesia:write(R),
				R
	end,
	case mnesia:transaction(F) of
		{atomic, Peer} ->
			{ok, Peer};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec find_peer(Identity) -> Result
	when
		Identity :: string() | binary(),
		Result :: {ok, #dsc_peer{}} | {error, Reason},
		Reason :: not_found | term().
%% @doc Find a peer by identity.
%%
find_peer(Id) when is_list(Id) ->
	find_peer(list_to_binary(Id));
find_peer(Id) when is_binary(Id) ->
	F = fun() ->
				mnesia:read(dsc_peer, Id, read)
	end,
	case mnesia:transaction(F) of
		{atomic, [#dsc_peer{} = Peer]} ->
			{ok, Peer};
		{atomic, []} ->
			{error, not_found};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec delete_peer(Identity) -> ok
	when
		Identity :: string() | binary().
%% @doc Delete an entry from the `dsc_peer' table.
delete_peer(Id) when is_list(Id) ->
	delete_peer(list_to_binary(Id));
delete_peer(Id) when is_binary(Id) ->
	F = fun() ->
		mnesia:delete(dsc_peer, Id, write)
	end,
	case mnesia:transaction(F) of
		{atomic, _} ->
			ok;
		{aborted, Reason} ->
			exit(Reason)
	end.


-spec add_realm(Name, ApplicationId, Mode, Peers, Expiration) -> Result
	when
		Name :: string() | binary(),
		ApplicationId :: non_neg_integer(),
		Mode :: local | relay | proxy | redirect,
		Peers :: list(),
		Expiration :: undefined | calendar:datetime(),
		Result :: {ok, #dsc_realm{}} | {error, Reason},
		Reason :: term().
%% @doc Create an entry in `dsc_realm' table.
%%
add_realm(Name, AppId, Mode, Peers, Expr) when is_list(Name),
		is_integer(AppId), is_atom(Mode), is_list(Peers) ->
add_realm(list_to_binary(Name), AppId, Mode, Peers, Expr); 
add_realm(Name, AppId, Mode, Peers, Expr) when is_binary(Name),
		is_integer(AppId), is_atom(Mode), is_list(Peers) ->
	F = fun() ->
				R = #dsc_realm{name = Name, app_id = AppId, mode = Mode,
						peers = Peers, expiration = Expr},
				ok = mnesia:write(R),
				R
	end,
	case mnesia:transaction(F) of
		{atomic, Realm} ->
			{ok, Realm};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec find_realm(Name) -> Result
	when
		Name :: string() | binary(),
		Result :: {ok, #dsc_realm{}} | {error, Reason},
		Reason :: not_found | term().
%% @doc Find a realm by name
%%
find_realm(Name) when is_list(Name) ->
	find_realm(list_to_binary(Name));
find_realm(Name) when is_binary(Name) ->
	F = fun() ->
				mnesia:read(dsc_realm, Name, read)
	end,
	case mnesia:transaction(F) of
		{atomic, [#dsc_realm{} = Realm]} ->
			{ok, Realm};
		{atomic, []} ->
			{error, not_found};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec delete_realm(Name) -> ok
	when
		Name :: string() | binary().
%% @doc Delete an entry from the `dsc_realm' table.
delete_realm(Name) when is_list(Name) ->
	delete_realm(list_to_binary(Name));
delete_realm(Name) when is_binary(Name) ->
	F = fun() ->
		mnesia:delete(dsc_realm, Name, write)
	end,
	case mnesia:transaction(F) of
		{atomic, _} ->
			ok;
		{aborted, Reason} ->
			exit(Reason)
	end.

-spec start_service(Address, Port, Options) -> Result
	when
		Address :: inet:ip_address(),
		Port :: inet:port_number(),
		Options :: list(),
		Result :: {ok, Pid} | {error, Reason},
		Pid :: pid(),
		Reason :: term().
%% @doc Start a DIAMETER relay service fsm supervisor.
start_service(Address, Port, Options) when is_tuple(Address), is_number(Port),
		is_list(Options) ->
	gen_server:call(dsc, {start, Address, Port, Options}).


%%---------------------------------------------------------------------
%%  Internal functions
%%---------------------------------------------------------------------

