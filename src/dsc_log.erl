%%% dsc_log.erl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2015-2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @doc This library module implements the functions used in logging in
%%% the {@link //dsc. dsc} application.
%%%

-module(dsc_log).

%% export dsc_log public API
-export([traffic_open/0, traffic_close/0]).
-export([traffic_log/4]).

-define(TRAFFICLOG, dsc_traffic).

%% support deprecated_time_unit()
-define(MILLISECOND, milli_seconds).
%-define(MILLISECOND, millisecond).

% calendar:datetime_to_gregorian_seconds({{1970,1,1},{0,0,0}})
-define(EPOCH, 62167219200).

%% define a traffic event
-type traffic_event() :: {
		TS :: pos_integer(), % posix time in milliseconds
		N :: pos_integer(), % unique number
		Node :: atom(),
		Service :: {inet:ip_address(), inet:port_number()},
		OriginPeer :: diameter_app:peer(),
		DestinationPeer :: diameter_app:peer(),
		Packet :: diameter_codec:packet()}.

%%----------------------------------------------------------------------
%%  The dsc_log public API
%%----------------------------------------------------------------------

-spec traffic_open() -> Result
	when
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc Open traffic event disk log.
traffic_open() ->
	{ok, Directory} = application:get_env(dsc, traffic_log_dir),
	{ok, LogSize} = application:get_env(dsc, traffic_log_size),
	{ok, LogFiles} = application:get_env(dsc, traffic_log_files),
	open_log(Directory, ?TRAFFICLOG, LogSize, LogFiles).

-spec traffic_log(Service, OrigPeer, DestPeer, Packet) -> Result
	when
		Service :: {inet:ip_address(), inet:port_number()},
		OrigPeer :: diameter_app:peer(),
		DestPeer :: diameter_app:peer(),
		Packet :: diameter_codec:packet(),
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc Write a DIAMETER traffic event to traffic log.
traffic_log(Service, OrigPeer, DestPeer, Packet) ->
	Event = [node(), Service, OrigPeer, DestPeer, Packet],
	write_log(?TRAFFICLOG, Event).

-spec traffic_close() -> Result
	when
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc Close traffice event disk log.
traffic_close() ->
	close_log(?TRAFFICLOG).

-spec open_log(Directory, Log, LogSize, LogFiles) -> Result
	when
		Directory  :: string(),
		Log :: atom(),
		LogSize :: integer(),
		LogFiles :: integer(),
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc open disk log file
open_log(Directory, Log, LogSize, LogFiles) ->
	case file:make_dir(Directory) of
		ok ->
			open_log1(Directory, Log, LogSize, LogFiles);
		{error, eexist} ->
			open_log1(Directory, Log, LogSize, LogFiles);
		{error, Reason} ->
			{error, Reason}
	end.
%% @hidden
open_log1(Directory, Log, LogSize, LogFiles) ->
	FileName = Directory ++ "/" ++ atom_to_list(Log),
	case disk_log:open([{name, Log}, {file, FileName},
					{type, wrap}, {size, {LogSize, LogFiles}}]) of
		{ok, Log} ->
			ok;
		{repaired, Log, _Recovered, _Bad} ->
			ok;
		{error, Reason} ->
			Descr = lists:flatten(disk_log:format_error(Reason)),
			Trunc = lists:sublist(Descr, length(Descr) - 1),
			error_logger:error_report([Trunc, {module, ?MODULE},
					{log, Log}, {error, Reason}]),
			{error, Reason}
	end.

-spec write_log(Log, Event) -> Result
	when
		Log :: atom(),
		Event :: list(),
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc write event into given log file
write_log(Log, Event) ->
	TS = erlang:system_time(?MILLISECOND),
	N = erlang:unique_integer([positive]),
	LogEvent = list_to_tuple([TS, N | Event]),
	disk_log:log(Log, LogEvent).

-spec close_log(Log) -> Result
	when
		Log :: atom(),
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc close log files
close_log(Log) ->
	case disk_log:close(Log) of
		ok ->
			ok;
		{error, Reason} ->
			Descr = lists:flatten(disk_log:format_error(Reason)),
			Trunc = lists:sublist(Descr, length(Descr) - 1),
			error_logger:error_report([Trunc, {module, ?MODULE},
					{log, Log}, {error, Reason}]),
			{error, Reason}
	end.


%%---------------------------------------------------------------------
%%  Internal functions
%%---------------------------------------------------------------------

