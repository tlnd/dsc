%%% dsc_api_SUITE.erl
%%% vim: ts=3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  @doc Test suite for public API of the {@link //dsc. dsc} application.
%%%
-module(dsc_api_SUITE).
-copyright('Copyright (c) 2018 SigScale Global Inc.').

%% common_test required callbacks
-export([suite/0, sequences/0, all/0]).
-export([init_per_suite/1, end_per_suite/1]).
-export([init_per_testcase/2, end_per_testcase/2]).

%% Note: This directive should only be used in test suites.
-compile(export_all).

-include("dsc.hrl").
-include_lib("common_test/include/ct.hrl").

%%---------------------------------------------------------------------
%%  Test server callback functions
%%---------------------------------------------------------------------

-spec suite() -> DefaultData :: [tuple()].
%% Require variables and set default values for the suite.
%%
suite() ->
	[{userdata, [{doc, "Test suite for public API in DSC"}]}].

-spec init_per_suite(Config :: [tuple()]) -> Config :: [tuple()].
%% Initialization before the whole suite.
%%
init_per_suite(Config) ->
	ok = dsc_test_lib:initialize_db(),
	ok = dsc_test_lib:start(),
	Config.

-spec end_per_suite(Config :: [tuple()]) -> any().
%% Cleanup after the whole suite.
%%
end_per_suite(Config) ->
	ok = dsc_test_lib:stop(),
	Config.

-spec init_per_testcase(TestCase :: atom(), Config :: [tuple()]) -> Config :: [tuple()].
%% Initialization before each test case.
%%
init_per_testcase(_TestCase, Config) ->
	Config.

-spec end_per_testcase(TestCase :: atom(), Config :: [tuple()]) -> any().
%% Cleanup after each test case.
%%
end_per_testcase(_TestCase, _Config) ->
	ok.

-spec sequences() -> Sequences :: [{SeqName :: atom(), Testcases :: [atom()]}].
%% Group test cases into a test sequence.
%%
sequences() -> 
	[].

-spec all() -> TestCases :: [Case :: atom()].
%% Returns a list of all test cases in this test suite.
%%
all() -> 
	[add_service, find_nonexisting_service, find_existing_service, delete_service,
		add_peer, find_nonexisting_peer, find_existing_peer, delete_peer,
		add_realm, find_nonexisting_realm, find_existing_realm, delete_realm].

%%---------------------------------------------------------------------
%%  Test cases
%%---------------------------------------------------------------------

add_service() ->
	[{userdata, [{doc, "add a service to dsc_service table"}]}].

add_service(_Config) ->
	Name = rand:uniform(100000),
	Address = dsc_test_lib:ipv4(),
	Port = dsc_test_lib:port(),
	Enabled = true,
	Options = [],
	{ok, Svc} = dsc:add_service(Name, Address, Port, Options, Enabled),
	#dsc_service{name = Name, address = Address, port = Port, enabled = Enabled,
			options = Options} = Svc.

find_nonexisting_service() ->
	[{userdata, [{doc, "Attempt to find non existing service in dsc_service table"}]}].

find_nonexisting_service(_Config) ->
	Name = rand:uniform(100000),
	{error, not_found} = dsc:find_service(Name).

find_existing_service() ->
	[{userdata, [{doc, "Attempt to find existing service in dsc_service table"}]}].

find_existing_service(_Config) ->
	Name = rand:uniform(100000),
	Address = dsc_test_lib:ipv4(),
	Port = dsc_test_lib:port(),
	Enabled = true,
	Options = [],
	{ok, _} = dsc:add_service(Name, Address, Port, Options, Enabled),
	{ok, Service} = dsc:find_service(Name),
	#dsc_service{name = Name, address = Address, port = Port, enabled = Enabled,
			options = Options} = Service.

delete_service() ->
	[{userdata, [{doc, "Delete a service in dsc_service table"}]}].

delete_service(_Config) ->
	Name = rand:uniform(100000),
	Address = dsc_test_lib:ipv4(),
	Port = dsc_test_lib:port(),
	Enabled = true,
	Options = [],
	{ok, _} = dsc:add_service(Name, Address, Port, Options, Enabled),
	{ok, _} = dsc:find_service(Name),
	ok = dsc:delete_service(Name),
	{error, not_found} = dsc:find_service(Name).

add_peer() ->
	[{userdata, [{doc, "Add a peer to dsc_peer table"}]}].

add_peer(_Config) ->
	Id = dsc_test_lib:random_str(),
	Active = dsc_test_lib:peer_active(),
	{Date, _} = calendar:universal_time(),
	Expiration = calendar:gregorian_days_to_date(
			calendar:date_to_gregorian_days(Date) + 30),
	Transport = dsc_test_lib:peer_transport(),
	{ok, Peer} = dsc:add_peer(Id, Active, Expiration, Transport),
	IdBin = list_to_binary(Id),
	#dsc_peer{identity = IdBin, active = Active, expiration = Expiration,
			transport = Transport} = Peer.

find_nonexisting_peer() ->
	[{userdata, [{doc, "Find a non existing peer in dsc_peer table"}]}].

find_nonexisting_peer(_Config) ->
	Id = dsc_test_lib:random_str(),
	{error, not_found} = dsc:find_peer(Id).

find_existing_peer() ->
	[{userdata, [{doc, "Find an  existing peer in dsc_peer table"}]}].

find_existing_peer(_Config) ->
	Id = list_to_binary(dsc_test_lib:random_str()),
	Active = dsc_test_lib:peer_active(),
	{Date, _} = calendar:universal_time(),
	Expiration = calendar:gregorian_days_to_date(
			calendar:date_to_gregorian_days(Date) + 30),
	Transport = dsc_test_lib:peer_transport(),
	{ok, _} = dsc:add_peer(Id, Active, Expiration, Transport),
	{ok, Peer} = dsc:find_peer(Id),
	#dsc_peer{identity = Id, active = Active, expiration = Expiration,
			transport = Transport} = Peer.

delete_peer() ->
	[{userdata, [{doc, "Delete a peer from dsc_peer table"}]}].

delete_peer(_Config) ->
	Id = dsc_test_lib:random_str(),
	State = dsc_test_lib:peer_active(),
	{Date, _} = calendar:universal_time(),
	Expiration = calendar:gregorian_days_to_date(
			calendar:date_to_gregorian_days(Date) + 30),
	Transport = dsc_test_lib:peer_transport(),
	{ok, _} = dsc:add_peer(Id, State, Expiration, Transport),
	{ok, _} = dsc:find_peer(Id),
	ok = dsc:delete_peer(Id),
	{error, not_found} = dsc:find_peer(Id).

add_realm() ->
	[{userdata, [{doc, "add a realm to dsc_realm table"}]}].

add_realm(_Config) ->
	Name = dsc_test_lib:random_str(),
	AppId = rand:uniform(100),
	Mode = dsc_test_lib:realm_mode(),
	Peers = [dsc_test_lib:random_str()],
	{Date, _} = calendar:universal_time(),
	Expiration = calendar:gregorian_days_to_date(
			calendar:date_to_gregorian_days(Date) + 30),
	{ok, Realm} = dsc:add_realm(Name, AppId, Mode, Peers, Expiration),
	NameB = list_to_binary(Name),
	#dsc_realm{name = NameB, app_id = AppId, mode = Mode, peers = Peers,
			expiration = Expiration} = Realm.

find_nonexisting_realm() ->
	[{userdata, [{doc, "Find a non existing realm in dsc_realm table"}]}].

find_nonexisting_realm(_Config) ->
	Name = dsc_test_lib:random_str(),
	{error, not_found} = dsc:find_realm(Name).

find_existing_realm() ->
	[{userdata, [{doc, "Find an existing realm in dsc_realm table"}]}].

find_existing_realm(_Config) ->
	Name = list_to_binary(dsc_test_lib:random_str()),
	AppId = rand:uniform(100),
	Mode = dsc_test_lib:realm_mode(),
	Peers = [dsc_test_lib:random_str()],
	{Date, _} = calendar:universal_time(),
	Expiration = calendar:gregorian_days_to_date(
			calendar:date_to_gregorian_days(Date) + 30),
	{ok, _} = dsc:add_realm(Name, AppId, Mode, Peers, Expiration),
	{ok, Realm} = dsc:find_realm(Name),
	#dsc_realm{name = Name, app_id = AppId, mode = Mode, peers = Peers,
			expiration = Expiration} = Realm.

delete_realm() ->
	[{userdata, [{doc, "Find an existing realm in dsc_realm table"}]}].

delete_realm(_Config) ->
	Name = dsc_test_lib:random_str(),
	AppId = rand:uniform(100),
	Mode = dsc_test_lib:realm_mode(),
	ServerId = dsc_test_lib:random_str(),
	{Date, _} = calendar:universal_time(),
	Expiration = calendar:gregorian_days_to_date(
			calendar:date_to_gregorian_days(Date) + 30),
	{ok, _} = dsc:add_realm(Name, AppId, Mode, ServerId, Expiration),
	{ok, _} = dsc:find_realm(Name),
	ok = dsc:delete_realm(Name),
	{error, not_found} = dsc:find_realm(Name).

%%---------------------------------------------------------------------
%%  Internal functions
%%---------------------------------------------------------------------

